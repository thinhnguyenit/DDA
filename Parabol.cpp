#include "Parabol.h"
#include <iostream>
using namespace std;
void Draw2Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	//draw 2 points
	int new_x;
	int new_y;
	//1
	new_x = xc + x;
	new_y = yc + y;
	SDL_RenderDrawPoint(ren, new_x, new_y);
	//2
	new_x = xc - x;
	new_y = xc + y;
	SDL_RenderDrawPoint(ren, new_x, new_y);
}
void BresenhamDrawParabolPositive(int xc, int yc, int A, SDL_Renderer *ren)
{
	int x, y, p;
	x = 0;
	y = 0;
	p = 1 - A;
	Draw2Points(xc, yc, x, y, ren);
	while (x <= A)
	{
		if (p <= 0)
		{
			p += 2 * x + 3;
		}
		else
		{
			p += 2 * x + 3 - 2 * A;
			y--;
		}
		x++;
		Draw2Points(xc, yc, x, y, ren);
	}
	p = 2 * A - 1;
	Draw2Points(xc, yc, x, y, ren);
	while (x < 720 && y < 1280)
	{
		if (p <= 0)
		{
			p += 4 * A;
		}
		else
		{
			p += 4 * A - 4 * x - 4;
			y++;
		}
		x--;
		Draw2Points(xc, yc, x, y, ren);
	}
}

void BresenhamDrawParabolNegative(int xc, int yc, int A, SDL_Renderer *ren)
{
	//1
	int x, y, p;
	x = 0;
	y = 0;
	p = 1 - A;
	Draw2Points(xc, yc, x, y, ren);
	while (x <= A)
	{
		if (p <= 0)
		{
			p += 2 * x + 3;
		}
		else
		{
			p += 2 * x + 3 - 2 * A;
			y--;
		}
		x++;
		Draw2Points(xc, yc, x, y, ren);
	}
	//2
	p = 2 * A - 1;
	Draw2Points(xc, yc, x, y, ren);
	while (x < 720 && y < 1280)
	{
		if (p <= 0)
		{
			p += 4 * A;
		}
		else
		{
			p += 4 * A - 4 * x - 4;
			x++;
		}
		y--;
		Draw2Points(xc, yc, x, y, ren);
	}
}
